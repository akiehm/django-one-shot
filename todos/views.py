from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todolists/list.html"

class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todolists/detail.html"

class TodoCreateView(CreateView):
    model = TodoList
    template_name="todolists/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todolists_list")

    #def form_valid(self, form):