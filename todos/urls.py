from django.urls import path

from todos.views import (TodoListView, TodoDetailView, TodoCreateView,)

urlpatterns = [
    path("", TodoListView.as_view(), name="todolists_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todolist_detail"),
    path("create/", TodoCreateView.as_view(), name="todolist_create"),
    path("items/create/", TodoCreateView.as_view(), name="todoitem_create"),#TODO: Update view class
    path("<int:pk>/edit/", TodoCreateView.as_view(), name="todolist_edit"),#TODO: Create edit view
    path("<int:pk>/delete/", TodoCreateView.as_view(), name="todolist_delete"),#TODO: Update delete view
    path("items/<int:pk>/edit/", TodoCreateView.as_view(), name="todoitem_edit"),#TODO: Create item edit view
]